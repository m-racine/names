import unittest
import mlfey_names.names as names

class FixGameTests(unittest.TestCase):
    def test_clean_game(self):
        self.assertEqual("test-game-name",names.fixGame("Test Game Name"))

    def test_non_string(self):
        with self.assertRaises(TypeError):
            names.fixGame(["This","is",5])

    def test_all_characters(self):
        test_string = "[Biscotti: The mountain remembers?]"
        self.assertEqual("biscotti-the-mountain-remembers",names.fixGame(test_string))

class HelperTests(unittest.TestCase):
    pass

class CheckNestedTests(unittest.TestCase):
    pass

class MainTests(unittest.TestCase):
    pass

class FlattenTests(unittest.TestCase):
    pass

class SquashTests(unittest.TestCase):
    pass

class FuseFillsTests(unittest.TestCase):
    pass

class SubAndTests(unittest.TestCase):
    pass

class SubAndHelperTests(unittest.TestCase):
    pass

class AddEndingsTests(unittest.TestCase):
    pass

class GenNamesTests(unittest.TestCase):
    # def test_contains_and(self):
    #     test_string = u'Everything And Started to Fall'
    #     print names.gen_names(test_string)
    #     self.assertTrue(False)

    def test_start_with_and(self):
        test_string = u'And Everything Started to Fall'
        self.assertTrue(names.gen_names(test_string))
        #terrible test, basically just "does it run"

class GenMetacriticNamesTests(unittest.TestCase):
    pass
