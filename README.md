names is a helper library used for generating variants of
names of games (and likely other things) based on some observed
trends to assist to searching across different sites representations
of those names.

The general entrypoints are the "gen_names", "gen_metacritic_names" and "fixGame" functions. The first two return an array of possible names that an input string could be represented by, the third
is used for removing special characters from game names that
cannot be represented in URLs.

To test:

python -m unittest